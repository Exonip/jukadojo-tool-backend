package de.tillgrossmann.jdtool.backend.repository;

import de.tillgrossmann.jdtool.backend.entities.DeleteLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeleteRepository extends JpaRepository<DeleteLog, Long> {
}
