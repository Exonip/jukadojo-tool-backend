package de.tillgrossmann.jdtool.backend.repository;

import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UploadRepository extends JpaRepository<UploadSave, Long> {
}
