package de.tillgrossmann.jdtool.backend.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class DeleteLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String name;

    long deletedId;

    @Column(nullable = true, updatable = false)
    private LocalDateTime deletedAt;

    @PrePersist
    protected void onCreate() {
        deletedAt = LocalDateTime.now();
    }

}
