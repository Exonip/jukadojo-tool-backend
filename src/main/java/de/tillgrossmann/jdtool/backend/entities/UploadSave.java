package de.tillgrossmann.jdtool.backend.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class UploadSave {

    public static final String UPLOAD_DIR = "./uploads";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;

    @Column(nullable = true, updatable = false)
    private LocalDateTime createdAt;

    @Transient
    public String generateFileName() {
        return id + "_" + firstName + "_" + lastName + ".png";
    }

    @PrePersist
    protected void onCreate() {
        createdAt = LocalDateTime.now();
    }
}