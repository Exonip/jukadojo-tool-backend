package de.tillgrossmann.jdtool.backend.controller;

import de.tillgrossmann.jdtool.backend.services.GetService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/get-upload")
@RequiredArgsConstructor
public class GetController {

    private final GetService getService;


    @GetMapping("/{id}")
    public ResponseEntity<Resource> getUploadImage(@PathVariable Long id) {
        Resource imageResource = getService.getUploadImageById(id);

        MediaType contentType = MediaType.IMAGE_JPEG;

        return ResponseEntity.ok()
                .contentType(contentType)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + imageResource.getFilename() + "\"")
                .body(imageResource);
    }
}
