package de.tillgrossmann.jdtool.backend.controller;

import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import de.tillgrossmann.jdtool.backend.services.SearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/search-uploads")
public class SearchController {
    private final SearchService searchService;

    @GetMapping("/{search}")
    public List<UploadSave> searchUploads(@PathVariable String search) {
        return searchService.searchUploadsByFullName(search);
    }

}
