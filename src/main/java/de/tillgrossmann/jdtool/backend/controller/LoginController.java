package de.tillgrossmann.jdtool.backend.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @GetMapping("/login")
    public LoginResponse login() {
        return new LoginResponse("Success");
    }

    public record LoginResponse(String message) {

    }
}
