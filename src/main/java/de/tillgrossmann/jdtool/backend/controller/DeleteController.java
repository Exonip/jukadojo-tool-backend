package de.tillgrossmann.jdtool.backend.controller;

import de.tillgrossmann.jdtool.backend.services.DeleteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/delete-upload")
@RequiredArgsConstructor
public class DeleteController {

    private final DeleteService deleteService;


    @GetMapping("/{id}")
    public String deleteImage(@PathVariable Long id) {
        return deleteService.deleteUpload(id);
    }
}
