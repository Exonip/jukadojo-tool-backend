package de.tillgrossmann.jdtool.backend.security;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AuthExtractor {
    public static final String AUTH_TOKEN_HEADER_NAME = "X-API-KEY";


    @Value("${security.apikey}")
    private String authKey;

    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        String apiKey = request.getHeader(AUTH_TOKEN_HEADER_NAME);
        if ((apiKey != null) && (apiKey.equals(authKey))) {
            return Optional.of(new ApiKeyAuthentication(apiKey, AuthorityUtils.NO_AUTHORITIES));
        }
        return Optional.empty();
    }
}
