package de.tillgrossmann.jdtool.backend.services;

import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import de.tillgrossmann.jdtool.backend.repository.UploadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearchService {


    private final UploadRepository uploadRepository;


    public List<UploadSave> searchUploadsByFullName(String fullName) {
        List<UploadSave> allUploads = uploadRepository.findAll(); // Assuming a method to get all uploads from the repository

        // Filter uploads based on the full name where the first or last name starts with the search string
        List<UploadSave> filteredUploads = allUploads.stream()
                .filter(upload -> upload.getFirstName().toLowerCase().startsWith(fullName.toLowerCase())
                        || upload.getLastName().toLowerCase().startsWith(fullName.toLowerCase())
                        || (upload.getFirstName() + " " + upload.getLastName()).toLowerCase().startsWith(fullName.toLowerCase())
                        || (upload.getLastName() + " " + upload.getFirstName()).toLowerCase().startsWith(fullName.toLowerCase())
                )
                .limit(5)
                .collect(Collectors.toList());

        return filteredUploads;
    }

}
