package de.tillgrossmann.jdtool.backend.services;

import de.tillgrossmann.jdtool.backend.entities.DeleteLog;
import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import de.tillgrossmann.jdtool.backend.repository.DeleteRepository;
import de.tillgrossmann.jdtool.backend.repository.UploadRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static de.tillgrossmann.jdtool.backend.entities.UploadSave.UPLOAD_DIR;

@Service
@RequiredArgsConstructor
public class DeleteService {

    private final UploadRepository uploadRepository;
    private final DeleteRepository deleteRepository;

    public String deleteUpload(Long id) {
        UploadSave upload = uploadRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("UploadSave with id " + id + " not found"));

        // Create a delete log
        DeleteLog deleteLog = new DeleteLog();
        deleteLog.setDeletedId(upload.getId());
        deleteLog.setName(upload.getFirstName() + " " + upload.getLastName());
        deleteRepository.save(deleteLog);

        // Delete the file from the file system
        Path path = Paths.get(UPLOAD_DIR + File.separator + upload.generateFileName());
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new RuntimeException("Failed to delete file: " + path, e);
        }

        // Delete the upload entry from the database
        uploadRepository.delete(upload);

        return "deleted";
    }
}