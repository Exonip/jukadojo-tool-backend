package de.tillgrossmann.jdtool.backend.services;

import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import de.tillgrossmann.jdtool.backend.repository.UploadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static de.tillgrossmann.jdtool.backend.entities.UploadSave.UPLOAD_DIR;

@Service
@RequiredArgsConstructor
public class GetService {


    private final UploadRepository uploadRepository;


    public Resource getUploadImageById(Long id) {
        Optional<UploadSave> uploadOptional = uploadRepository.findById(id);

        if (uploadOptional.isPresent()) {
            UploadSave upload = uploadOptional.get();
            Path imagePath = Paths.get(UPLOAD_DIR + "/" + upload.generateFileName());

            try {
                Resource resource = new UrlResource(imagePath.toUri());
                if (resource.exists() || resource.isReadable()) {
                    return resource;
                } else {
                    throw new RuntimeException("Could not read the image file!");
                }
            } catch (IOException e) {
                throw new RuntimeException("Error occurred while reading the image file!", e);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image with ID " + id + " not found!");
        }
    }


}
