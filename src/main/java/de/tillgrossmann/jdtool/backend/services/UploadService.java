package de.tillgrossmann.jdtool.backend.services;

import de.tillgrossmann.jdtool.backend.entities.UploadSave;
import de.tillgrossmann.jdtool.backend.repository.UploadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static de.tillgrossmann.jdtool.backend.entities.UploadSave.UPLOAD_DIR;

@Service
@RequiredArgsConstructor
public class UploadService {


    private final UploadRepository uploadRepository;


    public String uploadFile(MultipartFile file, String firstName, String lastName) throws IOException {
        if (file.isEmpty()) {
            return "File is empty";
        }


        UploadSave upload = new UploadSave();
        upload.setFirstName(firstName);
        upload.setLastName(lastName);
        uploadRepository.save(upload);

        Path path = Paths.get(UPLOAD_DIR + File.separator + upload.generateFileName());
        Files.write(path, file.getBytes());

        return "File uploaded successfully";

    }


}
