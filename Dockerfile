# Use the arm64 version of the Ubuntu base image
FROM arm64v8/ubuntu:latest
LABEL authors="exonip"

# Use the arm64 version of the OpenJDK 17 JDK slim image
FROM arm64v8/openjdk:17-jdk-slim
COPY build/libs/jdtool-backend-0.0.1-SNAPSHOT.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]